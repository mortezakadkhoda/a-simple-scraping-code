import requests
from bs4 import BeautifulSoup
import re
url = "https://www.bahesab.ir/time/"
headers = {'User-Agent':"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/102.0.0.0 Safari/537.36"}

page = requests.get(url, headers= headers)
if page.status_code == 200:
    soup = BeautifulSoup(page.content, "html.parser")

    script = soup.find("script").string

    time_dict = re.search(r'"H":(\d.),"M":(\d.),"S":(\d.)',script_tag)
    time= {"H":time_dict.group(1),"M":time_dict.group(2),"S":time_dict.group(3)}
    print(f"{time['H']}:{time['M']}:{time['S']}")
else:
    print(f"problem to give information from {url} ,error:{page.status_code}")